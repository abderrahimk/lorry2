use std::{
    path::{Path, PathBuf},
    sync::Arc,
};

use axum::{extract::State, Json};
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use utils::command_wrapper::CommandBuilder;

use super::state_db;

/// Path to the endpoint to update the controller configuration.
pub const PATH: &str = "/1.0/read-configuration";

/// Pull configuration from the remote configuration repository and update configuration based on it.
#[axum_macros::debug_handler]
#[tracing::instrument]
pub(crate) async fn read_config(
    State((db, app_settings)): State<(Arc<state_db::StateDatabase>, super::ControllerSettings)>,
) -> Result<Json<String>, ReadConfigError> {
    get_confgit(&app_settings)
        .await
        .map_err(ReadConfigError::FailedToFetchConfgit)?;

    let config_obj = match read_config_file(&app_settings) {
        Ok(v) => Ok(v),
        Err(e) => Err(ReadConfigError::FailedToReadConfigFile(e)),
    }?;

    let lorries_to_remove = db.get_lorries_paths().await?;
    let mut m = std::collections::HashSet::new();
    for i in lorries_to_remove {
        m.insert(i);
    }
    let mut lorries_to_remove = m;

    for section in config_obj {
        match section {
            Config::Lorries(l) => {
                let added = add_matching_lorries_to_statedb(&db, l, &app_settings)
                    .await
                    .map_err(|e| ReadConfigError::FailedToAddLorriesToDB(Box::new(e)))?;
                lorries_to_remove = &lorries_to_remove - &added;
            }
        }
    }

    futures::future::join_all(
        lorries_to_remove
            .iter()
            .map(|path| async { db.remove_lorry(path).await }),
    )
    .await
    .into_iter()
    .collect::<Result<Vec<_>, _>>()
    .map_err(ReadConfigError::FailedToRemoveLorries)?;

    //TODO we may have a redirect in the request, and must handle it?

    Ok("Configuration has been updated!".to_string().into())
}

impl axum::response::IntoResponse for ReadConfigError {
    fn into_response(self) -> axum::response::Response {
        let (status, err_msg) = match self {
            ReadConfigError::FailedToFetchConfgit(e) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Failed to fetch config git repo: {:?}", e),
            ),
            ReadConfigError::FailedToReadConfigFile(e) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Failed to read config: {:?}", e),
            ),
            ReadConfigError::FailedToAddLorriesToDB(e) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Failed to add lorries to DB: {:?}", e),
            ),
            ReadConfigError::FailedToRemoveLorries(e) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Failed to remove unneccessary lorries from DB: {:?}", e),
            ),
            ReadConfigError::FailedDBTransaction(e) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("DB transaction failed: {:?}", e),
            ),
        };

        (status, err_msg).into_response()
    }
}

#[derive(thiserror::Error, Debug)]
enum AddLorryToDbError {
    #[error("DB transation failed")]
    DBOperationFailed(sqlx::Error),
}

#[tracing::instrument]
async fn add_matching_lorries_to_statedb(
    db: &state_db::StateDatabase,
    l: LorryConfig,
    app_settings: &super::ControllerSettings,
) -> Result<std::collections::HashSet<super::comms::LorryPath>, AddLorryToDbError> {
    let filenames = l
        .globs
        .into_iter()
        .map(|i| {
            let l = app_settings
                .configuration_directory
                .join(CONFIG_FILENAME)
                .parent()
                .expect("We should have a parent, we JUST APPENDED SOEMTHING TO IT")
                .join(i); //This may seem a bit circuiticious; the rationalse is that CONFIG_FILENAME may have multiple components, so the call to parent() won't necessarily just pop it off

            tracing::debug!("path= {:?}", &l);
            l
        })
        .map(|s| glob::glob(s.to_str().expect("Path cannot be globbed over...")))
        .flat_map(|s| s.expect("globbing failed"))
        .collect::<Vec<_>>();
    tracing::debug!("filenames: {:?}", filenames);
    //TODO sort `filenames` rather than just iterating over it. Then again, is that strictly necessary?

    let mut lorry_specs = filenames
        .into_iter()
        .filter_map(|f| f.ok())
        .filter_map(|filename| {
            tracing::debug!(
                "Reading .lorry file: {:?}",
                filename.canonicalize().expect("unwrap issue")
            );
            //we "merely" log failures to read a lorry spec rather than stop the whole operation, since *some* of the specs may be OK
            match get_valid_lorry_specs(filename) {
                Ok(b) => {
                    tracing::debug!("Read the lorry spec fine");
                    Some(b)
                }
                Err(e) => {
                    tracing::error!(
                        endpoint =%PATH,
                        "There was an error trying to read the lorry spec: {}",
                        e
                    ); //Just skip the spec; we shouldn't return an Error if just *one* lorry is malformed, we should process the rest!
                    None
                }
            }
        })
        .flat_map(|b| b.into_iter())
        .collect::<Vec<_>>();
    tracing::debug!("read lorry specs: {:?}", lorry_specs);
    //TODO do we actually need to sort lorry_specs? If we don't need to, we can avoid the `collect` call
    lorry_specs.sort_by(|(s1, _), (s2, _)| s1.cmp(s2));

    let interval = l.interval;
    let timeout = l.timeout;
    let prefix = &l.prefix;

    let added_paths = lorry_specs.into_iter().map(|(subpath, obj)| async move {
        let path = super::comms::LorryPath::new(format!("{}/{}", &prefix, subpath));

        let mut b = std::collections::BTreeMap::new();
        b.insert(&path, obj);
        let spec = b;
        db.add_to_lorries(&path, &spec, "", "", interval, timeout)
            .await
            .map_err(AddLorryToDbError::DBOperationFailed)?; //TODO perhaps this fn should just take a asingle object?

        //Ok::<_, AddLorryToDbError>(path)
        Ok(path)
    });

    let added_paths = futures::future::join_all(added_paths)
        .await
        .into_iter()
        .collect::<Result<Vec<_>, _>>()?;

    Ok(added_paths
        .into_iter()
        .collect::<std::collections::HashSet<_>>())
}

fn get_valid_lorry_specs(
    filename: PathBuf,
) -> Result<std::collections::BTreeMap<String, workerlib::LorrySpec>, Box<dyn std::error::Error>> {
    let s = std::fs::read_to_string(filename)?;

    Ok(workerlib::extract_lorry_specs(s)?)
}

const CONFIG_FILENAME: &str = "lorry-controller.conf";
fn read_config_file(
    app_settings: &super::ControllerSettings,
) -> Result<Vec<Config>, Box<dyn std::error::Error>> {
    let filename = app_settings.configuration_directory.join(CONFIG_FILENAME);
    tracing::debug!("Reading configuration file {:?}", filename);

    match std::fs::read_to_string(&filename) {
        Ok(s) => match serde_yaml::from_str::<Vec<Config>>(&s) {
            Ok(j) => Ok(j),
            Err(e) => {
                tracing::error!(endpoint = %PATH, "Error parsing config: {}", e);
                Err(Box::new(e)) //TODO test this error case
            }
        },
        Err(e) => {
            tracing::error!(
                endpoint = %PATH,"There was an error accessing the lorry-controller.conf, is your config repo correctly formed?: {}", e);
            Err(Box::new(e)) //TODO test this error case
        }
    }
}

//TODO  verify that we actually use other types, becuase looking at gnome it seems we don't
/// Configuration block contained in `lorry-controller.conf`, specifying a set of mirrors
/// and information about how to run them
#[derive(Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum Config {
    #[serde(rename = "lorries")]
    Lorries(LorryConfig),
}

/// Configuration block corresponding to a set of lorry mirrors.
#[derive(Serialize, Deserialize, Debug)]
pub struct LorryConfig {
    //TODO give default value of 1 day
    pub interval: super::comms::Interval,
    pub prefix: String,
    pub globs: Vec<String>,
    //TODO give default value of 1 day
    //TODO is this value even used anywhere?
    //If not, we should drop it from the DB too
    pub timeout: super::comms::Interval,
}

#[derive(thiserror::Error, Debug)]
pub(crate) enum ReadConfigError {
    #[error("An error occurred when trying to mirror the upstream configuration git repo")]
    FailedToFetchConfgit(Box<dyn std::error::Error>),

    #[error("An error occurred when trying to read and  parse the configurationo")]
    FailedToReadConfigFile(Box<dyn std::error::Error>),

    #[error("An error occurred when trying to add a lorry config to the database")]
    FailedToAddLorriesToDB(Box<dyn std::error::Error>),

    #[error("An error occurred removing unneeded lorries from config")]
    FailedToRemoveLorries(sqlx::Error),

    #[error(transparent)]
    FailedDBTransaction(#[from] sqlx::Error),
}

#[tracing::instrument]
async fn get_confgit(
    app_settings: &super::ControllerSettings,
) -> Result<(), Box<dyn std::error::Error>> {
    let confdir = &app_settings.configuration_directory;
    if !confdir.exists() {
        git_clone_confgit(app_settings, confdir).await
    } else {
        update_confgit(app_settings, confdir).await
    }
}
#[tracing::instrument]
async fn update_confgit(
    app_settings: &super::ControllerSettings,
    confdir: &Path,
) -> Result<(), Box<dyn std::error::Error>> {
    tracing::info!("Updating CONFGIT in {:?}", &confdir);

    CommandBuilder::new("git")
        .args(&["reset", "--hard"])
        .execute(&confdir, |s| async move { tracing::debug!(s) })
        .await?;

    CommandBuilder::new("git")
        .args(&["clean", "-fdx"])
        .execute(&confdir, |s| async move { tracing::debug!(s) })
        .await?;

    CommandBuilder::new("git")
        .args(&["remote", "update", "origin"])
        .execute(&confdir, |s| async move { tracing::debug!(s) })
        .await?;
    CommandBuilder::new("git")
        .args(&[
            "reset",
            "--hard",
            &format!("origin/{}", &app_settings.confgit_branch),
        ])
        .execute(&confdir, |s| async move { tracing::debug!(s) })
        .await?;

    Ok(())
}

#[tracing::instrument]
async fn git_clone_confgit(
    app_settings: &super::ControllerSettings,
    confdir: &Path,
) -> Result<(), Box<dyn std::error::Error>> {
    let url = &app_settings.confgit_url;
    let branch = &app_settings.confgit_branch;
    tracing::info!("Cloning {} to {:?}", url, &confdir);
    CommandBuilder::new("git")
        .args(&["clone", "-b"])
        .arg(branch)
        .arg(url)
        .arg(confdir)
        .execute(
            std::env::current_dir()?,
            |s| async move { tracing::debug!(s) },
        )
        .await?;
    Ok(())
}
