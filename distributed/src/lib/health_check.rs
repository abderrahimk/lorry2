//! Test that the subcomponents of the controller are working properly.
//! Currently this just means checking the DB connection is alive, but should be expanded if
//! more subsystems are added at a later date.

use std::sync::Arc;

use axum::{extract::State, response::IntoResponse};

use crate::state_db;

///Path to the health check endpoint
pub const PATH: &str = "/1.0/health-check";

/// Check that all the components of the controller are working properly
#[axum_macros::debug_handler]
#[tracing::instrument]
pub(crate) async fn health_check(
    State((db, app_settings)): State<(Arc<state_db::StateDatabase>, super::ControllerSettings)>,
) -> Result<(), HealthError> {
    //Test the DB connection is working
    db.test_connection()
        .await
        .map_err(HealthError::DBConnectionBroken)?;

    Ok(())
}

pub enum HealthError {
    DBConnectionBroken(sqlx::Error),
}

impl IntoResponse for HealthError {
    fn into_response(self) -> axum::response::Response {
        let (status, err_msg) = match self {
            HealthError::DBConnectionBroken(e) => (
                http::StatusCode::INTERNAL_SERVER_ERROR,
                format!("Problem with database connection: {:?}", e),
            ),
        };

        tracing::error!(endpoint =%PATH, "Health check failed: {}", err_msg); // does it actually make sense to do this, given it will try ot log to DB and... fail?

        (status, err_msg).into_response()
    }
}
