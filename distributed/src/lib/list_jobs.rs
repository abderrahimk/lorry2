use std::sync::Arc;

use super::state_db;
use axum::{extract::State, http::StatusCode, Json};

/// Path to the endpoint to display all mirroing job's history
pub const PATH: &str = "/1.0/list-jobs";

/// Respond with a list of all the jobs that have ever been run in this controller's history,
#[axum_macros::debug_handler]
#[tracing::instrument]
pub(crate) async fn list_all_jobs(
    State((db, app_settings)): State<(Arc<state_db::StateDatabase>, super::ControllerSettings)>,
) -> Result<Json<Vec<JobResult>>, ListAllJobsAsJsonError> {
    //TODO: perhaps pre-sort to put running jobs up top and then sort by date issued?
    get_all_jobs(&db, app_settings)
        .await
        .map_err(ListAllJobsAsJsonError::DBError)
        .map(Json::from)
}
#[derive(thiserror::Error, Debug)]
pub(crate) enum ListAllJobsAsJsonError {
    #[error("Error occurred during database operation")]
    DBError(#[from] sqlx::Error),
}

impl axum::response::IntoResponse for ListAllJobsAsJsonError {
    fn into_response(self) -> axum::response::Response {
        let (status, err_msg) = match self {
            ListAllJobsAsJsonError::DBError(e) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Database operation getting the jobs failed: {:?}", e),
            ),
        };

        tracing::error!(endpoint =%PATH, "Error at listing jobs: {}", err_msg);

        (status, err_msg).into_response()
    }
}

pub(crate) async fn list_all_jobs_pretty(
    State((db, app_settings)): State<(state_db::StateDatabase, super::ControllerSettings)>,
) -> () {
    todo!("How do I return HTML: I assume I need to make a template but not 100% sure how to do that and this is not a priority")
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct JobResult {
    pub id: super::comms::JobId,
    pub path: String,
    pub exit_status: super::comms::JobExitStatus,
    pub host: String,
}

#[tracing::instrument]
async fn get_all_jobs(
    db: &state_db::StateDatabase,
    app_settings: super::ControllerSettings,
) -> Result<Vec<JobResult>, sqlx::Error> {
    //let now = db.get_current_time().await;
    Ok(db
        .get_individual_job_records()
        .await?
        .into_iter()
        .collect::<Vec<_>>())
}
