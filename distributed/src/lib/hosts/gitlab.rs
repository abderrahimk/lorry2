use futures::StreamExt;
use futures::TryStreamExt;
use gitlab::api::AsyncQuery;

use crate::comms;
use crate::downstream;

#[derive(Clone, Debug)]
pub struct GitLabDownstream {
    pub downstream_visibility: gitlab::api::common::VisibilityLevel,
    pub gitlab_instance: gitlab::AsyncGitlab,
}

trait GitlabErrorReport {
    fn report(self, why: impl Into<String>) -> Self;
}

impl<T, E> GitlabErrorReport for Result<T, E>
where
    E: std::error::Error,
{
    fn report(self, why: impl Into<String>) -> Self {
        if let Err(e) = &self {
            let why = why.into();
            tracing::error!("{why} - {e} - {e:?}");
        }
        self
    }
}

impl GitLabDownstream {
    #[tracing::instrument]
    pub(crate) async fn prepare_repo(
        &self,
        repo_path: &comms::LorryPath,
        metadata: downstream::RepoMetadata,
    ) -> Result<(), Box<dyn std::error::Error + Send>> {
        match gitlab::api::projects::Project::builder()
            .project(repo_path)
            .build()
            .report(format!("building project {repo_path}"))
            .map_err(|e| Box::new(e) as Box<dyn std::error::Error + Send>)?
            .query_async(&self.gitlab_instance)
            .await
            .report(format!("querying project {repo_path}"))
        {
            Ok::<gitlab::types::Project, _>(project) => {
                tracing::info!("Project {repo_path} exists in local GitLab already.");

                //Update the description to match that from the upstream repo, if necessary
                if metadata.description != project.description {
                    if let Some(new_description) = metadata.description {
                        match gitlab::api::projects::EditProject::builder()
                            .project(repo_path)
                            .description(&new_description)
                            .build()
                            .report(format!(
                                "building editproject for {repo_path}: {new_description}"
                            )) {
                            Ok(query) => {
                                query
                                    .query_async(&self.gitlab_instance)
                                    .await
                                    .report(format!(
                                        "running editproject for {repo_path}: {new_description}"
                                    ))
                                    // handle errors by reporting and continuing on; updating project description is not critical!
                                    .map_or_else(
                                        |e| {
                                            tracing::warn!(
                                                "Failed to update description for {}: {e}",
                                                repo_path
                                            )
                                        },
                                        |_: ()| (),
                                    );
                            }
                            Err(e) => tracing::warn!(
                                "Failed to create update query for {}: {e}",
                                repo_path
                            ),
                        }
                    }
                }

                if project.default_branch.is_some() {
                    if metadata.head != project.default_branch && metadata.head.is_some() {
                        let new_head = metadata.head.unwrap();
                        //create the branch in case it does not already exist

                        match gitlab::api::projects::repository::branches::CreateBranch::builder()
                            .project(repo_path)
                            .branch(&new_head)
                            .build()
                            .report(format!("building createbranch {repo_path}:{new_head}"))
                        {
                            Ok(q) => q
                                .query_async(&self.gitlab_instance)
                                .await
                                .report(format!("running createbranch {repo_path}:{new_head}"))
                                .ok()
                                .unwrap_or(()), //it's OK to fail this, that would just mean the branch already exists; that or networkk issues which shouldn't be a crashing issue. Plus a this could be updated by the incoming pull. PLUS it's just the default branch, not actual content
                            Err(e) => tracing::warn!(
                                "Failed to create query to create a branch for {}: {e}",
                                repo_path
                            ),
                        };

                        match gitlab::api::projects::EditProject::builder()
                            .project(repo_path)
                            .default_branch(&new_head)
                            .build()
                            .report(format!("building editproject {repo_path}:{new_head}"))
                        {
                            Ok(q) => q
                                .query_async(&self.gitlab_instance)
                                .await
                                .report(format!("running editproject {repo_path}:{new_head}"))
                                .ok()
                                .unwrap_or(()),
                            Err(e) => tracing::warn!(
                                "Failed to create query to set default branch for {}: {e}",
                                repo_path
                            ),
                        }
                        Ok(())
                    } else {
                        // Either the downstream and upstream defrault brnaches agree, which is good, we don't need to do anything
                        // Or, the upstream default branch is not set. I have no idea what would cause that but I guess we'll just chug along anyway
                        //TODO seriously what would that case mean? Is it even possible?
                        Ok(())
                    }
                } else {
                    //the downstream has no default branch. That's OK, that should just mean we haven't pulled from upstream yet. Once we do that we'll get a default branch.
                    // So we return OK on the basis that we're about to pull to the downstream, which will shouldn't be obstructed by this fact, and will fix it
                    Ok(())
                }
            }

            Err(e) => {
                tracing::info!("Need to create {repo_path} on local GitLab due to err: {e}");
                let path_components = repo_path.components();

                if path_components.len() < 2 {
                    tracing::error!("Project path {repo_path} is too short!");
                    return Err(Box::new(PrepareRepoError::ProjectPathTooShort)
                        as Box<dyn std::error::Error + Send>);
                }

                let namespace_id = self.create_parent_groups(&path_components).await?;
                let created_project: gitlab::Project = {
                    let existing_project: Result<gitlab::Project, _> =
                        gitlab::api::projects::Project::builder()
                            .project(repo_path)
                            .build()
                            .report(format!("building project for get {repo_path}"))
                            .map_err(|e| Box::new(e) as Box<dyn std::error::Error + Send>)?
                            .query_async(&self.gitlab_instance)
                            .await
                            .report(format!("getting project {repo_path}"));
                    match existing_project {
                        Ok(ex) => Ok(ex),
                        Err(_) => {
                            let mut b = gitlab::api::projects::CreateProject::builder();
                            b.name(*(path_components.last().unwrap()))
                                .visibility(self.downstream_visibility)
                                .namespace_id(namespace_id.value());
                            b.path(*(path_components.last().unwrap()));

                            //TODO perhaps supply worker::DEFAULT_BRANCH_NAME if metadata.head is None? But that would not line up with the upstream...
                            //TODO we need to create a branch befor we set it as default!

                            if let Some(head) = metadata.head {
                                //create the branch before we set it as default, or we'll get an error
                                let _: Option<()> =
                                gitlab::api::projects::repository::branches::CreateBranch::builder()
                                    .project(repo_path)
                                    .branch(&head)
                                    .build()
                                    .report(format!("build createbranch for new project {repo_path}:{head}"))
                                    .map_err(|e| Box::new(e) as Box<dyn std::error::Error + Send>)?
                                    .query_async(&self.gitlab_instance)
                                    .await
                                    .report(format!("run createbranch for new project {repo_path}:{head}"))
                                    .ok();
                                b.default_branch(head);
                            }

                            metadata
                                .description
                                .map(|description| b.description(description));
                            b.pages_access_level(
                                gitlab::api::projects::FeatureAccessLevelPublic::Disabled,
                            )
                            .container_registry_enabled(false)
                            .autoclose_referenced_issues(false)
                            .lfs_enabled(true)
                            .auto_devops_enabled(false);

                            b
                                .build()
                                .report(format!("build createproject for new project {repo_path}"))
                                .map_err(|e| Box::new(e) as Box<dyn std::error::Error + Send>)?
                                .query_async(&self.gitlab_instance)
                                .await
                                .report(format!("run createproject for new project {repo_path}"))
                        }
                    }
                }
                .report(format!("acquire created project {repo_path}"))
                .map_err(|e| Box::new(e) as Box<dyn std::error::Error + Send>)?;

                tracing::info!("Created {} project in local Gitlab.", repo_path);
                gitlab::api::projects::EditProject::builder()
                    .project(created_project.id.value())
                    .issues_access_level(gitlab::api::projects::FeatureAccessLevel::Disabled)
                    .merge_requests_access_level(
                        gitlab::api::projects::FeatureAccessLevel::Disabled,
                    )
                    .builds_access_level(gitlab::api::projects::FeatureAccessLevel::Disabled)
                    .wiki_access_level(gitlab::api::projects::FeatureAccessLevel::Disabled)
                    .snippets_access_level(gitlab::api::projects::FeatureAccessLevel::Disabled)
                    .build()
                    .report(format!("building editproject for {repo_path}"))
                    .map_err(|e| Box::new(e) as Box<dyn std::error::Error + Send>)?
                    .query_async(&self.gitlab_instance)
                    .await
                    .report(format!("Running editproject for {repo_path}"))
                    .map_err(|e| Box::new(e) as Box<dyn std::error::Error + Send>)?;

                tracing::info!("Set up fully {} project in local Gitlab.", repo_path);
                Ok(())
            }
        }
    }

    /// Go through the path components. For each path component, make sure that the respective group exists, to create the full group tree, in preperation for creating a project at the most deeply nested group
    #[tracing::instrument]
    async fn create_parent_groups(
        &self,
        path_components: &Vec<&str>,
    ) -> Result<gitlab::GroupId, Box<dyn std::error::Error + Send>> {
        struct Group {
            full_path: String,
            id: gitlab::GroupId,
        }

        let r = futures::stream::iter(path_components[0..(path_components.len() - 1)].iter())
            .map(Ok::<_, Box<dyn std::error::Error + Send>>)
            .try_fold(
                Group {
                    full_path: "".to_string(),
                    id: gitlab::GroupId::new(0),
                }, //These are dummy placeholder values that will be replaced in the first iteration of the fold.
                //TODO THIS IS STILL A GUARD VALUE THOUGH FIX IT UP TO BE CLEARER
                |parent_group: Group, &group_name| async move {
                    let group_path = if parent_group.full_path.is_empty() {
                        group_name.to_string()
                    } else {
                        format!("{}/{group_name}", parent_group.full_path)
                    };

                    tracing::debug!("Creating group {group_path} if necessary");

                    let group: Group = match gitlab::api::groups::Group::builder()
                        .group(group_path.as_str())
                        .build()
                        .report(format!("building group get for {group_path}"))
                        .map_err(|e| Box::new(e) as Box<dyn std::error::Error + Send>)?
                        .query_async(&self.gitlab_instance)
                        .await
                        .report(format!("running group get for {group_path}"))
                    {
                        Ok::<gitlab::Group, _>(g) => {
                            tracing::debug!("Group {} was found!", &g.full_path);
                            Ok(Group {
                                full_path: g.full_path,
                                id: g.id,
                            })
                        }

                        Err(e) => match e {
                            // TODO I am really unsure about this. I used to check for ApiError::GitlabService, but that was failing in testing
                            // Looking at the docs, it seems we can get the "group not found" message in several different enum variants
                            // i.e GitLabService allowed me to specify an error code!
                            // so in conclusion TODO Make sure that we cover all the possible ways Gitlab can tell us we need to create this group
                            gitlab::api::ApiError::Gitlab { msg: _ } => {
                                tracing::debug!("trying to create group {group_path}...");
                                let r: gitlab::Group = {
                                    let mut b = gitlab::api::groups::CreateGroup::builder();
                                    b.name(group_name);
                                    b.path(group_name);
                                    if !parent_group.full_path.is_empty() {
                                        b.parent_id(parent_group.id.value()); //There is a real parent group so make the new group a subgroup of it
                                    }
                                    b.visibility(self.downstream_visibility);

                                    b.build()
                                        .report(format!("Building creategroup for {group_name}"))
                                        .map_err(|e| {
                                            Box::new(e) as Box<dyn std::error::Error + Send>
                                        })?
                                        .query_async(&self.gitlab_instance)
                                        .await
                                        .report(format!("Running creategroup for {group_name}"))
                                }
                                .map_err(|e| Box::new(e) as Box<dyn std::error::Error + Send>)?;

                                Ok(Group {
                                    full_path: r.full_path,
                                    id: r.id,
                                })
                            }

                            e => {
                                tracing::debug!(
                                    "Got some other critical error:{e}. Debug form: {:?}",
                                    e
                                );
                                Err(Box::new(e))
                            }
                        },
                    }
                    .map_err(|e| Box::new(e) as Box<dyn std::error::Error + Send>)?;

                    Ok(group)
                },
            )
            .await?
            .id;

        Ok(r)
    }
}

#[derive(thiserror::Error, Debug)]
enum PrepareRepoError {
    #[error("Project path contains less than two components: This would attempt to create a project outside a group.")]
    ProjectPathTooShort,
}
