use std::sync::Arc;

use axum::extract::State;

use crate::comms;

/// Path to the error reporting endpoint.
pub const PATH: &str = "/1.0/errors";

/// Respond with a list of logged errors, both mirroring erros on the minion end, and internal ones.
#[axum_macros::debug_handler]
#[tracing::instrument]
pub(crate) async fn report_errors(
    State((db, app_settings)): State<(
        Arc<super::state_db::StateDatabase>,
        super::ControllerSettings,
    )>,
) -> Result<axum::Json<Vec<ErrorLog>>, ErrorReportError> {
    let r = db.fetch_errors().await?.into();
    Ok(r)
}

#[derive(thiserror::Error, Debug)]
pub(crate) enum ErrorReportError {
    #[error("Error occurred during database operation")]
    DBOperationError(#[from] sqlx::Error),
}

impl axum::response::IntoResponse for ErrorReportError {
    fn into_response(self) -> axum::response::Response {
        let (status, err_msg) = match self {
            ErrorReportError::DBOperationError(e) => (
                http::StatusCode::INTERNAL_SERVER_ERROR,
                format!("Database operation getting errors failed: {:?}", e),
            ),
        };

        tracing::error!(endpoint = %PATH, "Error at listing errors: {}", err_msg);

        (status, err_msg).into_response()
    }
}

/// A report from some error that had occured presiouly during the controller's runtime.
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct ErrorLog {
    /// The error message text.
    pub error_message: String,

    /// What endpoint was the controller executing when this error occurred?
    pub endpoint: String,

    /// At what time did this error occur
    pub timestamp: comms::TimeStamp,

    /// If this was a mirroring error, what mirror
    pub for_lorry: Option<comms::LorryPath>,
}
