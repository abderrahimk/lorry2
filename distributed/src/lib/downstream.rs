//TODO the way this is implemnted, it might make more sense to use a Trait Object instead of an enum?

use crate::comms;
use crate::hosts;

/// Connection to a downstream host.
#[derive(Clone, Debug)]
pub enum Downstream {
    GitLab(hosts::gitlab::GitLabDownstream),
    Local(hosts::local::LocalDownstream),
}

impl Downstream {
    /// Create the target git repository for a minion to mirror to.
    pub async fn prepare_repo(
        &self,
        repo_path: &comms::LorryPath,
        metadata: RepoMetadata,
    ) -> Result<(), Box<dyn std::error::Error + Send>> {
        match self {
            Downstream::GitLab(g) => g.prepare_repo(repo_path, metadata).await,
            Downstream::Local(l) => l.prepare_repo(repo_path, metadata).await,
        }
    }
}

/// Configuration for the connection to the downstream host. Can be converted to a `Downstream` to create the connection.
#[derive(Debug)]
pub enum DownstreamSetup {
    /// Use the local filesystem to host mirror repositories.
    Local {
        /// The directory to the folder containing mirrors
        base_dir: std::path::PathBuf,
    },
    /// Use a downstream git server running GitLab
    Gitlab {
        /// The base host name of the server.
        /// Please do not include a scheme identifier.
        hostname: String,

        /// Visibility setting for the created repositories
        downstream_visibility: Visibility,

        /// Private access token to use for access to the GitLab API. Make sure that this has full access to the server you are mirroring to!
        private_token: String,

        /// Permit insecure access to gitlab
        insecure: bool,
    },
}

//TODO perhaps clap::Parser will make this more consistent wrt how it is configured? i.e Make file and CLI the same
#[derive(clap::Subcommand, Debug, serde::Deserialize)]
#[serde(tag = "downstream")]
pub enum PartialDownstreamSetup {
    /// We don't want end-users to be able to select this option, it exists purely for testing purposes.
    #[command(skip)]
    #[serde(skip)]
    Local {
        base_dir: Option<std::path::PathBuf>,
    },
    /// Use a downstream git server running GitLab to host mirrors.
    #[command(name = "gitlab")]
    #[serde(rename = "gitlab")]
    Gitlab {
        /// The hostname of the server.
        /// Please do not include a scheme identifier.
        #[arg(long = "hostname")]
        #[serde(rename = "hostname")]
        hostname: Option<String>,

        /// Visibility setting for the created repositories
        #[arg(long = "visibility", value_enum)]
        #[serde(rename = "visibility")]
        downstream_visibility: Option<Visibility>,

        /// Private access token to use for access to the GitLab API. Make sure that this has full access to the server you are mirroring to!
        #[arg(long = "gitlab-private-token")]
        #[serde(rename = "gitlab-private-token")]
        private_token: Option<String>,

        /// Permit insecure access to gitlab (ie http rather than https)
        #[arg(long = "gitlab-insecure-http")]
        #[serde(rename = "gitlab-insecure-http")]
        insecure: Option<bool>,
    },
}

#[derive(thiserror::Error, Debug)]
pub enum DownstreamSetupError {
    #[error("Local downstream was supplied no directory")]
    NoBaseDir,

    #[error("Remote downstream has no valid hostname")]
    InvalidHostname,

    #[error("Remote downstream has not been given a valid visiblity option for its mirrors")]
    InvalidVisibility,

    #[error("Remote downstream was not supplied an access token")]
    NoToken,
}

impl TryFrom<PartialDownstreamSetup> for DownstreamSetup {
    type Error = DownstreamSetupError;

    fn try_from(value: PartialDownstreamSetup) -> Result<Self, Self::Error> {
        match value {
            PartialDownstreamSetup::Local { base_dir } => Ok(Self::Local {
                base_dir: base_dir.ok_or(DownstreamSetupError::NoBaseDir)?,
            }),
            PartialDownstreamSetup::Gitlab {
                hostname,
                downstream_visibility,
                private_token,
                insecure,
            } => {
                // gitlab API uses https so we should be OK
                Ok(Self::Gitlab {
                    hostname: hostname.ok_or(DownstreamSetupError::InvalidHostname)?,
                    downstream_visibility: downstream_visibility
                        .ok_or(DownstreamSetupError::InvalidVisibility)?,
                    private_token: private_token.ok_or(DownstreamSetupError::NoToken)?,
                    insecure: insecure.unwrap_or(false),
                })
            }
        }
    }
}

/// Visibility setting to use when creating a repo. While this is host-dependent,
/// the current implementation should be lowest common denominator.
#[derive(clap::ValueEnum, Copy, Clone, Debug, serde::Deserialize)]
pub enum Visibility {
    #[serde(rename = "public")]
    Public,

    #[serde(rename = "internal")]
    Internal,

    #[serde(rename = "private")]
    Private,
}

#[async_convert::async_trait]
impl async_convert::TryFrom<DownstreamSetup> for Downstream {
    type Error = gitlab::GitlabError;

    async fn try_from(value: DownstreamSetup) -> Result<Self, Self::Error> {
        match value {
            DownstreamSetup::Local { base_dir } => {
                Ok(Downstream::Local(hosts::local::LocalDownstream {
                    base_dir: base_dir,
                }))
            }
            DownstreamSetup::Gitlab {
                hostname,
                downstream_visibility,
                private_token,
                insecure,
            } => Ok(Downstream::GitLab(hosts::gitlab::GitLabDownstream {
                downstream_visibility: downstream_visibility.into(),
                gitlab_instance: if insecure {
                    gitlab::GitlabBuilder::new(hostname, private_token)
                        .insecure()
                        .build_async()
                        .await?
                } else {
                    gitlab::GitlabBuilder::new(hostname, private_token)
                        .build_async()
                        .await?
                },
            })),
        }
    }
}

impl From<Visibility> for gitlab::api::common::VisibilityLevel {
    fn from(value: Visibility) -> Self {
        match value {
            Visibility::Public => gitlab::api::common::VisibilityLevel::Public,
            Visibility::Internal => gitlab::api::common::VisibilityLevel::Internal,
            Visibility::Private => gitlab::api::common::VisibilityLevel::Private,
        }
    }
}

/// Metadata about the mirrored repo. Should be reasonably lowest common denominator
#[derive(Debug)]
pub struct RepoMetadata {
    /// The branch to use as the default branch of the repo
    pub head: Option<String>,
    /// Repo description to display to viewers
    pub description: Option<String>,
}
