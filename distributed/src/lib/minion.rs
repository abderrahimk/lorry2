use super::comms;
use rand::Rng;
use std::{str::FromStr, time::Duration};
use tokio::{task::JoinHandle, time::sleep};

/// Configuration for the minion program.
#[derive(Debug)]
pub struct MinionArguments {
    /// General mirroring settings.
    pub mirroring_settings: workerlib::Arguments,

    /// Controller's address.
    pub controller_address: reqwest::Url,

    /// Timout for requests made to the controller.
    pub controller_timeout: Duration,

    /// How long to wait for if the controller did not give a job.
    pub sleep: Duration,
}

impl MinionArguments {
    /// Initialise a config struct with default values, with an associated logging pipeline
    pub fn base() -> (Self, logging::Reciever) {
        let (ms, rx) = workerlib::Arguments::base();
        (
            Self {
                mirroring_settings: ms,
                controller_address: reqwest::Url::from_str("http://0.0.0.0:3000")
                    .expect("This is a valid IP address"),
                controller_timeout: Duration::from_secs(10),
                sleep: Duration::from_secs(rand::thread_rng().gen_range(30..=60)),
            },
            rx,
        )
    }
}

/// Run a minion.
/// * `args` - all the configuration necessary to run the minion program
/// * `reciever` - should be connected to the transmitter passed into `workerlib`'s logging functionality.
pub async fn work(args: MinionArguments, reciever: &mut logging::Reciever) {
    let args = std::sync::Arc::new(args);

    loop {
        tracing::info!("Getting a new job...");
        let new_job = get_new_job(args.as_ref()).await;
        match new_job {
            Some(j) => run_job(j, args.clone(), reciever).await,
            None => {
                //TODO use the proper logging interface
                tracing::info!(
                    "Got no job from the controller, sleeping for {}s",
                    args.sleep.as_secs()
                );
                sleep(args.sleep).await; //TODO could use TimerFuture, which works better with the async by descheduling a task. That said, there shouldn't be any other tasks!
            }
        }
    }
}

async fn run_job(
    j: comms::Job,
    args: std::sync::Arc<MinionArguments>,
    reciever: &mut logging::Reciever,
) {
    {
        //Test if we need to remove the internal repos becuase the controller believes they are corrupt
        let lorry_working_area = args
            .mirroring_settings
            .working_area
            .join(&j.lorry_name.replace('/', "_"));
        for repo in ["git-a", "git-b"] {
            //TODO should this functionality be placed into the worker library instead perhaps?
            let repo_path = lorry_working_area.join(repo);

            if !repo_path.exists() {
                break;
            }
            let fetch_head = repo_path.join("FETCH_HEAD");
            let delete_this_internal = fetch_head
                .metadata()
                .and_then(|m| m.modified())
                .map(|system_time| {
                    comms::TimeStamp::new(
                        chrono::DateTime::<chrono::Utc>::from(system_time).timestamp(),
                    )
                })
                .map(|last_modified| last_modified < j.purge_cutoff)
                .unwrap_or(false); //err on the side of not doing anything

            if delete_this_internal {
                match std::fs::remove_dir_all(repo_path) {
                    Ok(_) => {}
                    Err(e) => {
                        update_controller_about_job(
                            j.id,
                           format!( "Tried and failed to delete internal repo: we must not have the correct permsissions. Error was: {e}"),
                            comms::JobExitStatus::failure(get_lorry_disk_usage(&j.lorry_name, args.as_ref()).await),
                            args.as_ref(),
                        ).await;
                        return; // we shouldn't attempt to mirror further.
                    }
                }
            }
        }
    }

    let mut process_handle =
        create_mirroring_process(j.lorry_name.clone(), j.lorry_spec, args.clone());
    //TODO try to get around this clone; it exists almost entirely becuase kill_job takes the lorry name too - and that's not a commonly used path, while polling is going to happen all the time!

    loop {
        let (logging, exit_status) =
            poll_job(&mut process_handle, &j.lorry_name, args.as_ref(), reciever).await;
        //update the controller about the current job ststus
        let kill_signal =
            update_controller_about_job(j.id, logging, exit_status, args.as_ref()).await;
        //if the job completed, return
        tracing::debug!("exit_status:{:?}", exit_status);
        if exit_status.is_finished() {
            //do NOT poll process_handle here, this would be polling a completed future which will cause a crash
            //TODO There must be a more robust way to structure this; we're checking on  a second value to know things about the first.
            //TODO perhaps pass the future into poll job and pass out either a JobExitStatus or a handle?
            tracing::debug!("Worker finished!");
            break;
        }
        if kill_signal {
            tracing::debug!("Recieved signal to kill job");
            //we recieved the kill signal from the controller, kill the running process and clean up
            let exit = kill_job(process_handle, &j.lorry_name, args.as_ref()).await;
            args.mirroring_settings
                .transmitter
                .send((
                    logging::LogLevel::Debug,
                    "--RECEIVED KILL SIGNAL FROM CONTROLLER--".to_string(),
                ))
                .ok();
            reciever.close();
            let remaining_log = {
                let mut b = Vec::new();
                while let Ok((l, msg)) = reciever.try_recv() {
                    b.push(format!("{}:{}", l, msg));
                }
                b.join("\n")
            };

            update_controller_about_job(j.id, remaining_log, exit, args.as_ref()).await;
            break;
        }
    }
}

async fn kill_job(
    process_handle: JoinHandle<Result<(), workerlib::MirroringError>>,
    lorry_name: &str,
    args: &MinionArguments,
) -> comms::JobExitStatus {
    process_handle.abort();
    comms::JobExitStatus::failure(get_lorry_disk_usage(lorry_name, args).await)
    //TODO not too sure if this is correct
}

#[tracing::instrument]
///Retunrs true if we recieved the kill signal from the controller
async fn update_controller_about_job(
    job_id: comms::JobId,
    log: String,
    exit_status: comms::JobExitStatus,
    args: &MinionArguments,
) -> bool {
    let params_to_send = comms::StatusUpdate {
        job_id,
        exit_status,
        stdout: log,
    };

    match webapp_request(params_to_send, args).await {
        Err(e) => {
            tracing::error!("Error occurred when updating the controller :{}", e);
            false //If we somehow failed to contact the controller, just keep on working, it would make less sense to fail - how would you even tell the controller?.
        }
        Ok(body) => body.kill,
    }
}

///TODO with all these `tracing::error`s, I'm beginning to wonder if this should not just return a Result instead... But we can fail slightly less hard if we return a zero for disk usage
///TODO why does this allow negative values? How do I use negative disk space
/// FIXME this function is failing because we keep passing it  the job "path" which is it's path on the ustream, not its path in the local working area. This means it looks at the wrong (nonexistent) directory
async fn get_lorry_disk_usage(job_spec_path: &str, args: &MinionArguments) -> comms::DiskUsage {
    let dir_name = args
        .mirroring_settings
        .working_area
        .join(job_spec_path.replace('/', "_"));

    let (out, _err) = match utils::command_wrapper::CommandBuilder::new("du")
        .arg("-sk")
        .execute(&dir_name, |s| async move { tracing::debug!(s) })
        .await
    {
        Err(e) => {
            tracing::error!("du -sk {:?} failed: {:?}", dir_name, e);
            return comms::DiskUsage::new(0);
        }
        Ok((out, err)) => (out, err),
    };

    let last_word = match out.split_terminator('\n').last() {
        None => {
            tracing::warn!("No output from `du`");
            return comms::DiskUsage::new(0);
        }
        Some(last_line) => last_line.split_whitespace().next(),
    };

    tracing::debug!("last_word:{:?}", out);
    let usage = match last_word {
        None => {
            tracing::warn!("last line of `du` output was empty; can't get disk usage");
            0
        }
        Some(last_word) => last_word
            .parse::<i64>()
            .expect("`du` should never return a non-number value so this unwrap is OK"),
    };

    comms::DiskUsage::new(usage * 1024)
}

//TODO consider changing from a dyn Error into a specific error type
#[tracing::instrument]
async fn webapp_request<P>(
    params_to_send: P,
    args: &MinionArguments,
) -> Result<P::Response, Box<dyn std::error::Error>>
where
    P: comms::Communication,
{
    //TODO fill in an error type
    //TODO upgrade to HTTPS?
    let client = reqwest::Client::new(); //TODO do we need to make a new client every time?
    let mut url = args
        .controller_address
        .join(P::PATH)
        .expect("Could not create the full request URL");
    url.set_port(Some(args.controller_address.port().unwrap_or(80)))
        .unwrap();
    //FIXME unwrap above, we could error on a bad host
    //TODO What does it *mean* for the error type to be ()?
    tracing::debug!("Sending request to: {}", url);
    let request = client
        .post(url)
        //.header("Content-type", "application/x-www-form-urlencoded") //TODO this is not specifying JSON. The python version does not send JSON, but we are.
        .header("content-type", "application/json")
        .json(&params_to_send) //This shouldn't fail by user error, becuase the specification of Communication structs should be well-formed
        .timeout(args.controller_timeout);

    //get the response
    let response = request.send().await;

    match response {
        Ok(r) => Ok(r.json::<P::Response>().await?),
        Err(e) => Err(Box::new(e)),
    }
}

#[tracing::instrument]
async fn poll_job(
    process_handle: &mut JoinHandle<Result<(), workerlib::MirroringError>>,
    lorry_name: &str,
    args: &MinionArguments,
    reciever: &mut logging::Reciever,
) -> (String, comms::JobExitStatus) {
    if !process_handle.is_finished() {
        //The mirroring process is still running
        //fetch output from it
        let output =
        //TODO docs say that if the timout happens first, then the future is *cancelled*. Does this mean process_output is dropped,or that the next message is lost, or that it is reverted to the previous state?
            match tokio::time::timeout(Duration::from_secs(10), reciever.recv()).await {
                Ok(results) => match results {
                    Some((log_level, msg)) => format!("{}:{}\n", log_level, msg),
                    None => panic!("Minion message channel was closed! This shouldn't ever happen."), //The channel was closed. This should never happen, since it requires `worker` to panic (it's a library, it shouldn't), or be cancelled (we don't cancel it)
                },
                Err(_) => {
                    // There was no output to fetch from the task. TODO do verify re: cancellation mentioned above, how does it affect the process_output handle?
                    "".to_string()
                },
            };
        (output, comms::JobExitStatus::Running)
    } else {
        //miroring process finished

        //drain the message buffer and send everything to to the controller
        //TODO originally, this was done is 1024-byte chunks. why?
        let result = process_handle.await;

        let mirrors_failed = match result {
            Ok(results) => {results.err()}

            // Docs suggest this case occurs when the task was cancelled or panicked. 
            // We don't cancel our own tasks, so this only occurs during a panic.
            // We shouldn't get panics from the task, since it's just calling library code, which should *never* panic 
            // So this path should NEVER be hit
            Err(e) => panic!("Underlying `worker` task panicked or was cancelled. This should never happen.\n Error: {}",e),
        };
        tracing::debug!("Getting output logs");
        let mut output = vec![]; //TODO give this an initial capcity because we know that there can't be more than a certain amount of messages in the buffer
        while let Ok((lvl, msg)) = reciever.try_recv() {
            output.push(format!("{}:{}", lvl, msg));
        }

        tracing::debug!("Finished getting logs");
        let r = match mirrors_failed {
            None => {
                comms::JobExitStatus::success(get_lorry_disk_usage(lorry_name, args).await)
                //process suceeded,nothing to report!
            }
            Some(failed) => {
                //There were failures.
                //TODO what if the channel is full, these would halt forever...

                logging::progress(
                    "Mirror failed.", //TODO would we add a "Warn" level to logging to highlight these?
                    args.mirroring_settings.verbose_logging,
                    &args.mirroring_settings.transmitter,
                )
                .await;
                logging::progress(
                    format!("{:?}: {:?}", lorry_name, failed),
                    args.mirroring_settings.verbose_logging,
                    &args.mirroring_settings.transmitter,
                )
                .await;

                comms::JobExitStatus::failure(get_lorry_disk_usage(lorry_name, args).await)
                //TODO get an actual exit code from worker? Not that we were ever generating one...
            }
        };

        while let Ok((lvl, msg)) = reciever.try_recv() {
            output.push(format!("{}:{}", lvl, msg));
        }

        let output = output.join("\n");
        (output, r)
    }
}

fn create_mirroring_process(
    lorry_name: String,
    lorry_spec: workerlib::LorrySpec,
    args: std::sync::Arc<MinionArguments>,
) -> JoinHandle<Result<(), workerlib::MirroringError>> {
    //tracing::debug!("Running job {} on {}", j.job_id, j.path,);
    tokio::spawn(async move {
        workerlib::mirror(&lorry_name, &lorry_spec, &args.mirroring_settings).await
    })
}

///connect to the controller service specified in args.
///go to controller url /1.0/give-me-job. process the response.
///if it is a real job, return it, otherwise None
#[tracing::instrument]
async fn get_new_job(args: &MinionArguments) -> Option<comms::Job> {
    let params_to_send = comms::JobRequest {
        host: gethostname::gethostname()
            .to_str()
            .expect("My HostName is not a valid string.")
            .to_string(),
        pid: std::process::id(),
    };

    let response = webapp_request(params_to_send, args).await;

    match response {
        Ok(j) => j,
        Err(e) => {
            tracing::warn!(
                "There was an error getting a job from the controller: {}",
                e,
            );
            None
        }
    }
}
