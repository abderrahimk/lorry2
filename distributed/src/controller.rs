use clap::{arg, Parser};
use distributed::downstream::PartialDownstreamSetup;
use serde::Deserialize;
use std::{net::SocketAddr, path::PathBuf, str::FromStr};

#[derive(Parser, Debug, Deserialize)]
#[command(author, version, about, long_about = None)]

struct PartialControllerArguments {
    /// Path to the state database. If there is no database at this path, one will be created.
    #[arg(long = "statedb")]
    #[serde(rename = "statedb")]
    state_db: Option<String>, //TODO this could be a URL or a path, can we enforce this?

    /// Local directory populated with configuration files. This will be populated when the /read_config
    /// endpoint is called. Initially, this is not read, and the endpoint must be called for controller
    /// to read in the configuration. This includes information about what we want to mirror.
    #[arg(long = "configuration-directory")]
    #[serde(rename = "configuration-directory")]
    configuration_directory: Option<PathBuf>,

    /// Get the configuration git repo from this git URL; it will be mirrored to `configuration-directory`.
    /// This can be a local git repositority or a remote; so long as it is a git repo.
    #[arg(long = "confgit-url")]
    #[serde(rename = "confgit-url")]
    confgit_url: Option<String>, //TODO use a URL?

    /// Name of the branch on `confgit-url` that contains the configuration. The base config file should
    /// be called `lorry-controller.conf`, and in the root of the worktree.
    #[arg(long = "confgit-branch")]
    #[serde(rename = "confgit-branch")]
    confgit_branch: Option<String>,

    #[command(subcommand)]
    #[serde(flatten)]
    downstream: Option<distributed::downstream::PartialDownstreamSetup>,

    /// Controller listens on this port number (default:3000)
    #[arg(long = "port")]
    #[serde(rename = "port")]
    port: Option<u16>,
}

struct ControllerConfig {
    state_db: String,

    configuration_directory: PathBuf,

    confgit_url: String,

    confgit_branch: String,

    downstream: distributed::downstream::DownstreamSetup,

    port: u16,
}

impl From<ControllerCommandLineArguments> for ControllerConfig {
    fn from(value: ControllerCommandLineArguments) -> Self {
        let args = value.args;

        let read_file = value.config_file.and_then(|p| {
            let contents = match std::fs::read_to_string(&p) {
                Ok(content) => content,
                Err(e) => {
                    eprintln!("Could not read config file {}:\n {}", p.display(), e);
                    return None;
                }
            };

            let r: Result<PartialControllerArguments, _> = toml::from_str(&contents);
            if r.is_err() {
                eprintln!("The config file {} was invalid: {:?}", p.display(), r.err());
                return None;
            }
            r.ok()
        });
        let args = if let Some(read_file) = read_file {
            // Merge the downstream setup objects
            let d = match (args.downstream, read_file.downstream) {
                (None, None) => None,
                (None, Some(v)) => Some(v),
                (Some(v), None) => Some(v),
                (
                    Some(PartialDownstreamSetup::Local {
                        base_dir: base_dir_a,
                    }),
                    Some(PartialDownstreamSetup::Local {
                        base_dir: base_dir_b,
                    }),
                ) => Some(PartialDownstreamSetup::Local {
                    base_dir: base_dir_a.or(base_dir_b),
                }),
                (
                    Some(PartialDownstreamSetup::Gitlab {
                        hostname: host_url,
                        downstream_visibility,
                        private_token,
                        insecure,
                    }),
                    Some(PartialDownstreamSetup::Local { base_dir: _ }),
                ) => Some(PartialDownstreamSetup::Gitlab {
                    hostname: host_url,
                    downstream_visibility,
                    private_token,
                    insecure,
                }),
                (
                    Some(PartialDownstreamSetup::Local { base_dir }),
                    Some(PartialDownstreamSetup::Gitlab {
                        hostname: _,
                        downstream_visibility: _,
                        private_token: _,
                        insecure: _,
                    }),
                ) => Some(PartialDownstreamSetup::Local { base_dir }),

                (
                    Some(PartialDownstreamSetup::Gitlab {
                        hostname: host_url_a,
                        downstream_visibility: downstream_visibility_a,
                        private_token: private_token_a,
                        insecure: insecure_a,
                    }),
                    Some(PartialDownstreamSetup::Gitlab {
                        hostname: host_url_b,
                        downstream_visibility: downstream_visibility_b,
                        private_token: private_token_b,
                        insecure: insecure_b,
                    }),
                ) => Some(PartialDownstreamSetup::Gitlab {
                    hostname: host_url_a.or(host_url_b),
                    downstream_visibility: downstream_visibility_a.or(downstream_visibility_b),
                    private_token: private_token_a.or(private_token_b),
                    insecure: insecure_a.or(insecure_b),
                }),
            };
            PartialControllerArguments {
                state_db: args.state_db.or(read_file.state_db),
                configuration_directory: args
                    .configuration_directory
                    .or(read_file.configuration_directory),
                confgit_url: args.confgit_url.or(read_file.confgit_url),
                confgit_branch: args.confgit_branch.or(read_file.confgit_branch),
                downstream: d,
                port: args.port.or(read_file.port),
            }
        } else {
            args
        };

        ControllerConfig {
            state_db: args.state_db.expect("Must specify statedb"),
            configuration_directory: args
                .configuration_directory
                .unwrap_or(PathBuf::from_str(".").expect("this is a valid path")),
            confgit_url: args.confgit_url.expect("Must specify confgit-url"),
            confgit_branch: args.confgit_branch.expect("Must specify confgit-branch"),
            downstream: args
                .downstream
                .expect("Must specify a downstream")
                .try_into()
                .expect("Did not fully specify downstream"),
            port: args.port.unwrap_or(3000),
        }
    }
}

#[derive(Parser)]
struct ControllerCommandLineArguments {
    #[command(flatten)]
    args: PartialControllerArguments,

    #[arg(long = "config")]
    config_file: Option<PathBuf>,
}

#[tokio::main]
async fn main() {
    let args: ControllerConfig = ControllerCommandLineArguments::parse().into();

    let db_url = args.state_db; //TODO allow DB to be configurable: i.e could be SQLite, could be Postgres
                                //TODO fetch this from the enviroement vars or .env file rather than just the CLI.

    let settings: distributed::ControllerSettings = distributed::ControllerSettings {
        downstream: async_convert::TryInto::<distributed::downstream::Downstream>::try_into(
            args.downstream,
        )
        .await
        .expect("Could not connect to downstream"),
        configuration_directory: args.configuration_directory,
        confgit_url: args.confgit_url,
        confgit_branch: args.confgit_branch,
    };
    let addr = SocketAddr::from(([0, 0, 0, 0], args.port));
    let app = distributed::app(&db_url, settings).await;
    tracing::subscriber::set_global_default(app.logger).unwrap();
    let server_future = axum::Server::bind(&addr).serve(app.router.into_make_service());
    let db_writer_handler = app.writer;
    server_future.await.expect("Failed to run server..");
    db_writer_handler.await.expect("Failed to run logger"); //This doesn't matter, we only leave the server future if we are shut down externally anyway
}
