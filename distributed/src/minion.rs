use std::path::PathBuf;

use clap::Parser;
use distributed::minion::MinionArguments;
use serde::Deserialize;

#[derive(Parser, Debug, Deserialize, Clone)]
#[command(author, version, about, long_about = None)]
pub struct PartialMinionCommandLineArgs {
    #[command(flatten)]
    #[serde(flatten)]
    settings: workerlib::combine_configs::PartialArguments,

    /// The URL the controller is listening on
    #[arg(long = "controller-address")]
    #[serde(rename = "controller-address")]
    controller_address: Option<reqwest::Url>, //Default:0.0.0.0:3000

    /// The timout to set for requests to the controller (default: 10s)
    #[arg(long = "controller-timeout")]
    #[serde(rename = "controller-timeout")]
    timeout: Option<u64>, //default 10

    /// How long to wait between requests to the controller upon recieving a message that there are no jobs.
    /// Exlcuding this value, or setting it to 0, gives this a random value between 30 and 60 seconds.
    #[arg(long = "sleep")]
    #[serde(rename = "sleep")]
    sleep: Option<u64>,
}

//TODO Could use a rewrite on the doc comments here, given they are printed to the user when they ask for --help
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct MinionCommandLineArgs {
    #[command(flatten)]
    settings: PartialMinionCommandLineArgs,

    /// Can specify further configuration in this file. Options here correspond to the options that can be used on the command line
    #[arg(long = "config")]
    lorry_config_file: Option<PathBuf>,
}

impl From<MinionCommandLineArgs> for (MinionArguments, logging::Reciever) {
    fn from(value: MinionCommandLineArgs) -> (MinionArguments, logging::Reciever) {
        let config_fragments = value
            .lorry_config_file
            .and_then(|p| {
                let contents = match std::fs::read_to_string(&p) {
                    Ok(content) => content,
                    Err(e) => {
                        panic!("Could not read config file {}:\n {}", p.display(), e);
                    }
                };

                let r: Result<PartialMinionCommandLineArgs, _> = toml::from_str(&contents);
                if r.is_err() {
                    panic!("The config file {} was invalid: {:?}", p.display(), r.err());
                }
                r.ok()
            })
            .into_iter()
            .chain(std::iter::once(value.settings));
        let (acc, rx) = MinionArguments::base();
        let conf = config_fragments.fold(acc, |acc, cur| MinionArguments {
            mirroring_settings: workerlib::Arguments {
                working_area: cur
                    .settings
                    .working_area
                    .unwrap_or(acc.mirroring_settings.working_area),
                mirror_server_base_url: cur
                    .settings
                    .mirror_base_url
                    .unwrap_or(acc.mirroring_settings.mirror_server_base_url),
                pull_only: cur
                    .settings
                    .pull_only
                    .unwrap_or(acc.mirroring_settings.pull_only),
                verbose_logging: cur
                    .settings
                    .verbose
                    .unwrap_or(acc.mirroring_settings.verbose_logging),
                repack: cur.settings.repack.unwrap_or(acc.mirroring_settings.repack),
                keep_multiple_backups: cur
                    .settings
                    .keep_multiple_backups
                    .unwrap_or(acc.mirroring_settings.keep_multiple_backups),
                push_options: cur
                    .settings
                    .push_options
                    .unwrap_or(acc.mirroring_settings.push_options),
                check_ssl_certificates: cur
                    .settings
                    .check_certs
                    .unwrap_or(acc.mirroring_settings.check_ssl_certificates),
                transmitter: acc.mirroring_settings.transmitter,
            },
            controller_address: cur.controller_address.unwrap_or(acc.controller_address),
            controller_timeout: cur
                .timeout
                .map(std::time::Duration::from_secs)
                .unwrap_or(acc.controller_timeout),
            sleep: cur
                .sleep
                .map(std::time::Duration::from_secs)
                .unwrap_or(acc.sleep),
        });

        (conf, rx)
    }
}

#[tokio::main]
pub async fn main() {
    tracing_subscriber::fmt()
        .with_file(true)
        .with_line_number(true)
        .with_max_level(tracing::Level::DEBUG)
        .without_time()
        .compact()
        .try_init()
        .ok();

    let (args, mut rx) = MinionCommandLineArgs::parse().into();

    distributed::minion::work(args, &mut rx).await
}
