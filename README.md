# Lorry 2 Rust rewrite

This is a re-implementation of the [Lorry](https://gitlab.com/CodethinkLabs/lorry/lorry) tool in Rust. This rewrite differs from the original in that it drops various features that are not used by previous users:

- No cascading configuration files
- No bundling/tarballing of mirrors
- Only supports `git` and raw file upstreams.


It is currently not recommended you use this tool over the previous version as it is still not complete and subject to changes. Furthermore, it is not intended to function as a drop-in replacement.

# Dependencies

Requires Rust and Cargo. other dependencies are handled by `cargo`. To handle raw-file mirrors, you should also have Git LFS installed on the machine running `lorry` and on your upstream.

Currently we also depend on `curl`, but have an issue to remove that dependency.

# Usage


## Configuring the controller

The controller is a CLI program, with the following arguments:


* `--statedb` - URI pointing to the backing database. Currently, this only supports a `sqlite` database. The URI should include the `sqlite://` protocol identifier in this case.

* `--confgit-url` A URI to a git repository containing the information on what mirrors to manage.

* `--configuration-directory` The local directory to use when mirroring the git repository specified by `confgit-url`. Defaults to the current directory.

* `--confgit-branch` The branch of the repo at `confgit-url` to check for mirroring information.

* `--port` The port to listen on for endpoint requests and communicating with minions. This defaults to 3000.


You must also specify the downstream host. Currently, the only option is `gitlab`, which should be
supplied as the keyword by itself. This can be specified on the command line or the configuration file, and can
even be partially specified in one and the config completed in the other.

Selecting the `gitlab` downstream means the following options must also be supplied immediately after it:

* `--hostname` The host name for the downstream host server. This should agree with the minion's `--downstream-base-url` argument.

* `--visibility` The visibility setting to use when the controller creates a repository for minions to mirror to. Possible values are `public`, `internal` and `private`.

* `--gitlab-private-token` A token used for access to the gitLab API. Make sure that it has the correct permissions to operate in the host group specified in `host-url`, including the ability to create new subgroups and repositories.

* `--config` File path to a configuration file that can be used to specify further configuration not specified on the command line. 

### Config file

The config file specified by `--config` is a TOML file with the keys corresponding to the command-line argument names. Every option on the command line can be specified in this file, except for `--config` (You may not have config files build off one another recursively). Also note that if an option is given on the command line and in the config file, the value given on the command line takes precedence. In addition, if you wish to supply the downstream type, you must supply the identifier as the value in a key-value pair with the key "downstream" (see the example config below).

An example config file could look like this:

```
statedb = "sqlite://mydb.db"
confgit-url = "https://www.my-git-host.tld/confgit.git"
confgit-branch = "main"
downstream="gitlab"
host-url = "https://my-gitlab-instance.tld/mirror_group"
visibility = "public"
gitlab-private-token="GITLAB_ACCESS_TOKEN"
```


### CONFGIT

When the controller is run, the admin should send a GET request to its `/1.0/read-configuration` endpoint. This will fetch the configuration repository and read the configuration inside of it, informing the controller what mirrors it should manage. If this configuration is updated, the endpoint should be accessed again so that the controller can see the update.

The repository specifed by `confgit-url` (and `confgit-branch`) must contain a file called `lorry-controller.conf` at its root. This is a JSON file of the following form:
```
[
  {
    "type": "lorries",
    "interval": "foo",
    "timeout": "bar",
    "prefix": "group/subgroup/subsubgroup",
    "globs": [
      "folder/*.lorry"
    ]
  },
...
]
```

A list of maps, each corresponding to some set of mirrors that will be mirrored. Each set of mirrors will be mirrored into the same group. The fields of the map are as follows:

* `type` - should always be set to "lorries". This is some historical baggage and potentially should be removed.
* `interval` - a string in ISO8601 duration format. i.e "PT3H", meaning 3 hours. The mirrors this block specified will be attempted to be mirrored at the interval specified.
* `timeout` - a string in ISO8601 duration format, see `interval`. If a mirroring operation of a mirror in this repository takes longer than the specified duration, it will be cancelled.
* `prefix` - All the mirrors specified by this block will be mirrored under the path specified. This is appended to the mirror host base URI, so will generally correspond to the subgroup path the mirrors will be stored under
* `globs` - The list of mirror specification files that will be used for the mirroring operations. This can use glob syntax. For example, the `folder/*.lorry` example given will look for all `.lorry` files in the `folder` directory (relative to the `lorry-controller.conf` file)

Each mirror config file specified in `globs` will be mirrored to a subgroup under the paths listed in `prefix`, with a name corresponding to that of the file. A mirror config file is a YAML file listing some set of repositories to mirror. Each mirro specification takes the following form:
```
mirror-name:
    type: 
    ...
```

there can be multiple mirrors in a file, each listed as a seperate block. The name of each block (in the example, `mirror-name`) is the name of the repository that will be created under the group corresponding to the config file file, in which the mirror will be stored. The contents of each block depend on the type of repository being mirroed, which is decided by the `type` field, which has two possible values; `git` and `raw-files`.

In the case of `type: git`, the block has an extra `url` field, who's value is the URL of the remote git repository to be mirrored. Such a block would look like:

```
git-mirror-name:
    type: git
    url: http://my-git-host.tld/group/repo.git
```

In the case of `type: raw-file`, the block has a `urls` field, who's value is a list of blocks. Each block has two fields, `url`; the URL of the raw file to be mirrored, and `destination`; a relative path, which is where the file will be mirrored to relative to the mirror repository root. a raw-file block looks like:

```
raw-file-mirror-name:
    type: raw-file
    urls:
        - destination: first_file
          url: https://my-file-host.tld/directory/more-directory/file.tar
        - destination: another_file/subdir
          url: https://my-file-host.tld/directory/another_file.tar
```

Again, remember that a block in `lorry-controller.conf` can include multiple mirror config files, and a mirror config file can specify multiple mirror blocks.


## Configuring the minion(s)

The minion can be configured through command-line arguments, as well as a configuration TOML file that can be specified on the command line.

### Command line arguments
The following arguments can be supplied to the minion:

* `--working-area` The path the minion will use to store its internal intermediate repositories, from which will be pushed to the downstream. This defaults to `workd`

* `--downstream-base-url` The base URL of the downstream host. For example `git@gitlab.com:` If none is supplied, it will try to mirror to the local directory. The full path
of the target lorry will be appended to this before Git is asked to push to that
location.

* `--pull-only` If this is set (it is unset by default), the minion will not push its mirrors to a downstream. This only exists for debugging purposes, you probably should not use it.

* `--verbose` Also print mirroring logs to stderr, rather than just sending to the controller. disabled by default, logs are just sent to the controller.

* `--repack` Disabled by default. If set, runs `git gc` on updated repos before pushing downstream.

* `--keep-multiple-backups` Disabled by default. If set, stores timestamped backups of old versions of the internal repository.

* `--push-option` Can supply this argument multiple times. These values of this argument are used as axtra arguments when the minion calls `git push`.

* `--check-certs` If set, will check SSL certificates when download `raw-file` upstreams.

* `--controller-address` The URL of the controller the minion will try to communcate with to get mirroring jobs from. Defaults to `http://0.0.0.0:3000`

* `--controller-timeout` The timout (in seconds) for requests to the controller. 10 seconds if unset.

* `--sleep` The length of time (in seconds) the minion will wait for if it does not receive a job when requesting one from the controller. Unset or 0 values give this a random value between 30 and 60 seconds, chosen at start-time.

* `--config` The path to the configuration file to supply more configuration to the minion program. See the next section for information on the file. 


### Config file arguments

Arguments supplied on the command line take precedence over arguments supplied
in the config file. These are supplied in the TOML format, with keys corresponding
to the command-line arguments of the same name.

Any CLI argument can be provided in a config file, except for `--config` itself
(you cannot have config files recursively build on one another).

Note that `--push-option` can be supplied multiple times, as it can on the command
line, and is is concatenated with `--push-option`s set on the command line, rather
than being overwritten like other options.

An example config file would be:

```
downstream-base-url = "ssh://git@gitlab.com"
working-area = "data/working-area"
verbose = false
```


## DB setup instructions
To compile the distributed lorry system, you may need to rebuild the `sqlx-data.json` file, which contains information about the database necessary for SQLX to typecheck your code against. To do this, you will need to run `cargo sqlx prepare --merged` before compilation. Note that you may need to pass `--workspace`,instead, as SQLX are moving between the two flag names. 

If you're moving from the old python version of lorry-controller, you'll need to first convert the sqlite database backing lorry-controller from being managed by `yoyo` to being managed by` sqlx`. This is done by executing the `patch_column_types.sql` and `convert_from_yoyo.sql` queries on the database: 
```
sqlite3 lorries.db '.read patch_column_types.sql'
sqlite3 lorries.db '.read convert_from_yoyo.sql'
```


Contributions that add database migrations or create, modify, or remove queries will be required to regenerate the `sqlx-data.json` file. This can be done by running 
```sqlx database create
sqlx migrate run --source distributed/migrations```, to create a database with all the migrations, and then running `cargo sqlx prepare --merged`  as above to generate the JSON. The changes to this file should be tracked in Git.