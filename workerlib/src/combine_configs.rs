use std::path::PathBuf;

use clap::Parser;

use serde::Deserialize;

//TODO recent change means this can be changed quite a bit;
//shouldn't need unwraps, just use a defualt() Arguments as athe accumulator!
// Once we do that, we can move this up from the lib to the bin, since `lorry`
//bin is the only place using it

/// Combines the fields of the given configation fragments to get a full set of arguments to pass to the `mirror` function.
/// The most recent value for a given setting takes precedence. Should no value be provided, this function provides a sensible default.
/// Also returns, in order:
/// * A boolean indicating if to stop after the file parsing step of each lorry: this would be used to test if the files re valid without also trying to carry out the full mirror.
/// * Whether to use verbose debugging or not
/// * The logging target. TODO this should not be a string!
/// * The logging level limit - messages less important than this are discarded. //TODO but lorry doesn't need the full logginglevel struct!
pub fn combine_configs<T>(
    config_files: T,
) -> (
    crate::Arguments,
    bool,
    bool,
    Option<String>,
    Option<logging::LogLevel>,
    logging::Sender,
    logging::Reciever,
)
where
    T: IntoIterator<Item = PartialArguments>,
{
    let final_args = config_files
        .into_iter()
        .fold(PartialArguments::default(), |acc, cur| PartialArguments {
            working_area: cur.working_area.or(acc.working_area),
            mirror_base_url: cur.mirror_base_url.or(acc.mirror_base_url),
            pull_only: cur.pull_only.or(acc.pull_only),
            verbose: cur.verbose.or(acc.verbose),
            repack: cur.repack.or(acc.repack),
            keep_multiple_backups: cur.keep_multiple_backups.or(acc.keep_multiple_backups),
            push_options: cur.push_options.or(acc.push_options), //TODO should push-options be concatted rather than substituted? old cliapp code didn't, but maybe that is more logical
            check_certs: cur.check_certs.or(acc.check_certs),
            test_parsing_only: cur.test_parsing_only.or(acc.test_parsing_only),
            log: cur.log.or(acc.log),
            log_level: cur.log_level.or(acc.log_level),
        });
    let (tx, rx) = tokio::sync::mpsc::unbounded_channel::<(logging::LogLevel, String)>();
    //these unwraps are OK becuase all the defaults are non-None
    (
        crate::Arguments {
            working_area: ensure_path_exists(&final_args.working_area.unwrap()),
            mirror_server_base_url: final_args.mirror_base_url.unwrap(),
            pull_only: final_args.pull_only.unwrap(),
            verbose_logging: final_args.verbose.unwrap(),
            repack: final_args.repack.unwrap(),
            keep_multiple_backups: final_args.keep_multiple_backups.unwrap(),
            push_options: final_args.push_options.unwrap(),
            check_ssl_certificates: final_args.check_certs.unwrap(),
            transmitter: tx.clone(),
        },
        final_args.test_parsing_only.unwrap(),
        final_args.verbose.unwrap(),
        final_args.log,
        final_args.log_level,
        tx,
        rx,
    )
}
///creates the path if required and returns the absolute version of the path
fn ensure_path_exists(path: &std::path::PathBuf) -> std::path::PathBuf {
    std::fs::create_dir_all(path).unwrap();
    path.canonicalize().unwrap()
}
//TODO the partialarguments stuff can be moved to the binary, now that it's the on;y thing using this.
#[derive(Parser, Debug, Deserialize, Clone)]
#[command(author, version, about, long_about = None)]
pub struct PartialArguments {
    ///Directory for holding intermediate git repos and other internal data (default: workd/)
    #[arg(long = "working-area", short = 'w')]
    #[serde(rename = "working-area")]
    pub working_area: Option<PathBuf>,

    ///base URL to use for the mirror server
    #[arg(long = "downstream-base-url")]
    #[serde(rename = "downstream-base-url")]
    pub mirror_base_url: Option<String>,

    ///only pull from upstreams, do not push to mirror server (default:disabled)
    #[arg(long = "pull-only")]
    #[serde(rename = "pull-only")]
    pub pull_only: Option<bool>,

    ///report to STDOUT (default:disabled)
    #[arg(long = "verbose", short)]
    #[serde(rename = "verbose")]
    pub verbose: Option<bool>,

    ///repack git repos when an import has been updaed (default:enabled)
    #[arg(long = "repack")]
    #[serde(rename = "repack")]
    pub repack: Option<bool>,

    ///keep multiple timestamped backups (default:disabled)
    #[arg(long = "keep-multiple-backups")]
    #[serde(rename = "keep-multiple-backups")]
    pub keep_multiple_backups: Option<bool>,

    ///option for `git push` to pass to the downstream server
    #[arg(long = "push-option")]
    #[serde(rename = "push-option")]
    pub push_options: Option<Vec<String>>,

    ///validate SSL/TLS server certifications (default:enabled)
    #[arg(long = "check-certificates")]
    #[serde(rename = "check-certificates")]
    pub check_certs: Option<bool>,

    //TODO `test_parsing_only`, `log`, and `log_level` aren't used by minion, why are they here...
    ///validate `.lorry` files only, do not carry out mirroring operations. (default:disabled)
    #[arg(long = "test-parsing-only")]
    #[serde(rename = "test-parsing-only")]
    pub test_parsing_only: Option<bool>,

    /// The file to which to log to. Use "stderr" to output to standard error, or leave unspecified to suppress logging.
    /// options: "stderr", "none","syslog" (TODO not implemented), or a file path (TODO not implemented)
    #[arg(long = "log")]
    #[serde(rename = "log")]
    pub log: Option<String>,

    /// threshold of logging messages. Messages belew this level are suppressed
    /// One of "debug","info","warning","error","critical","fatal"
    /// TODO note that we only ever use "DEBUG", so we can probably drop or simplify this
    #[arg(long = "log-level")]
    #[serde(rename = "log-level")]
    pub log_level: Option<logging::LogLevel>,
}

impl Default for PartialArguments {
    fn default() -> PartialArguments {
        PartialArguments {
            working_area: Some(PathBuf::from("workd")),
            mirror_base_url: Some("".to_string()),
            pull_only: Some(false),
            verbose: Some(false),
            repack: Some(true),
            keep_multiple_backups: Some(false),
            push_options: Some(vec![]),
            check_certs: Some(true),
            test_parsing_only: Some(false),
            log: None,
            log_level: Some(logging::LogLevel::Info),
        }
    }
}
