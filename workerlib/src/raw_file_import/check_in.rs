use std::{fmt::Debug, fs, io::Write, path::PathBuf, process::Stdio};

use relative_path::{RelativePath, RelativePathBuf};
use thiserror::Error;

use utils::command_wrapper::{CommandBuilder, CommandExecutionError};

use super::{COMMITTER_EMAIL, COMMITTER_NAME};

use crate::DEFAULT_REF_NAME as BRANCH_REF;

#[derive(Debug, Error)]
pub enum CheckInError {
    #[error("Could not get the hash of the last commit in the repository")]
    CouldNotGetLastCommit { source: CommandExecutionError },
    #[error("Could not create the fast-import subprocess")]
    CouldNotCreateSubprocess {
        command: std::process::Command,
        source: std::io::Error,
    },
    #[error("Could not communicate with the fast-import subprocess")]
    CouldNotSendToSubprocess { source: std::io::Error },
    #[error("Could not get a git LFS pointer to the file we are trying to check in")]
    CouldNotGetPointerToFile { source: CommandExecutionError },
    #[error("Could not get the sha256 sum of the file we are tring to check in")]
    CouldNotGetShasum,
    #[error("Could not copy the file to the lfs objects directory")]
    CouldNotCopyDownloadedFile {
        source: std::io::Error,
        downloaded: PathBuf,
        destination: PathBuf,
    },
    #[error("We were given a bad relpath that tries to move out of the base dir")]
    UnsafePath { path: RelativePathBuf },

    #[error("Tried to extract the name of the file we are committing to LFS, but failed.")]
    CouldNotGetFilename { path: PathBuf },
    #[error("the git import process failed when we wait for it to finish")]
    ImportProcessFailedToClose { source: std::io::Error },
    #[error("the git import process ran into an error")]
    ImportProcessFailed,
}

/// Passes the commands to git lfs to import the file specified by `raw-file` into LFS for the repo at `gitdir`.
/// * `raw_file` - the path to the downloaded file to check in
///
/// * `destination` - the relative address to the repository root that the file will be placed into the FLS repository.
/// This should have been specified as `destination` in the raw-file lorry entry.
///
/// * `gitdir` - the internal repository we are currently operating in.
pub(crate) async fn check_file_into_lfs(
    raw_file: &PathBuf,
    destination: &RelativePath,
    gitdir: &crate::InternalGitDirectory,
    transmitter: &logging::Sender,
) -> Result<(), CheckInError> {
    let debug = |s: String| async move { logging::debug(&s, transmitter).await };

    //get the hash of the last commit
    let parsed_revisions = CommandBuilder::new("git")
        .arg("rev-parse")
        .arg(BRANCH_REF)
        .execute(gitdir, debug)
        .await;

    let latest_commit = if let Ok((out, _err)) = parsed_revisions {
        let out = out.trim();
        if out == BRANCH_REF || out.is_empty() {
            None
        } else {
            Some(out.to_owned())
        }
    } else {
        None
    };

    let mut import_command = std::process::Command::new("git");
    import_command
        .arg("fast-import")
        .arg("--quiet")
        .current_dir(gitdir)
        .stdin(Stdio::piped());

    let mut import_process =
        import_command
            .spawn()
            .map_err(|e| CheckInError::CouldNotCreateSubprocess {
                command: import_command,
                source: e,
            })?;
    let mut subprocess_stdin = import_process.stdin.take().expect("No STDIN?");

    if latest_commit.is_none() {
        //we could not find the latest commit. So we make one
        let commit_time = chrono::offset::Utc::now().timestamp().to_string();
        let commit = format!(
        "commit {BRANCH_REF}\ncommitter {COMMITTER_NAME} <{COMMITTER_EMAIL}> {commit_time} +0000\ndata <<EOM\nEnsure LFS is configured\nEOM\nM 100644 inline .gitattributes\ndata <<EOM\n* filter=lfs diff=lfs merge=lfs -text\n.gitattributes filter diff merge text=auto\nEOM\n\n"
    );
        subprocess_stdin
            .write_all(commit.as_bytes())
            .map_err(|e| CheckInError::CouldNotSendToSubprocess { source: e })?;
    }

    //get the git-lfs pointer  and sha256 sum of the file
    let pointer_digest = {
        CommandBuilder::new("git")
            .args(&["lfs", "pointer", "--file"])
            .arg(raw_file)
            .execute(gitdir, debug)
    }
    .await
    .map_err(|e| CheckInError::CouldNotGetPointerToFile { source: e })?
    .0; //TODO sometimes we get errors that are not errors, like "not updating refs/heads/master"
    let datasize = pointer_digest.len();

    let shasum = pointer_digest
        .lines()
        .nth(1)
        .and_then(|s| s.strip_prefix("oid sha256:"))
        .ok_or(CheckInError::CouldNotGetShasum)?;

    //Add the file to the repo
    let out_dir = gitdir.get_lfs_object_dir(shasum);
    fs::create_dir_all(&out_dir)
        .and_then(|_| fs::copy(raw_file, out_dir.join(shasum)))
        .map_err(|e| CheckInError::CouldNotCopyDownloadedFile {
            source: e,
            downloaded: raw_file.clone(),
            destination: out_dir.join(shasum),
        })?;

    //commit the changes made
    let commit_time = chrono::Utc::now().timestamp();
    let raw_file_basename = raw_file
        .file_name()
        .and_then(|x| x.to_str())
        .ok_or_else(|| CheckInError::CouldNotGetFilename {
            path: raw_file.clone(),
        })?;

    let full_path = destination.join_normalized(raw_file_basename);

    let path: &RelativePath = &full_path;

    if path
        .components()
        .take(1)
        .any(|c| c == relative_path::Component::ParentDir)
    //check that the first component isn't ../. if it is then the relative path is trying to break out of the working directory (I think)
    {
        return Err(CheckInError::UnsafePath {
            path: path.to_relative_path_buf(),
        });
    }

    let from_line = latest_commit.map_or("".to_string(), |l| format!("from {}\n", l));
    let commit_text = format!(
"commit {BRANCH_REF}\ncommitter {COMMITTER_NAME} <{COMMITTER_EMAIL}> {commit_time} +0000\ndata <<EOM\nimport {raw_file_basename}\nEOM\n{from_line}M 100644 inline {path}\ndata {datasize}\n{data}\n\n"
,raw_file_basename = raw_file_basename,path= path.as_str(), data = pointer_digest);

    subprocess_stdin
        .write_all(commit_text.as_bytes())
        .and_then(|_| subprocess_stdin.flush())
        .map_err(|e| CheckInError::CouldNotSendToSubprocess { source: e })?;
    drop(subprocess_stdin);
    let exit_status = import_process
        .wait()
        .map_err(|e| CheckInError::ImportProcessFailedToClose { source: e })?;
    if !exit_status.success() {
        Err(CheckInError::ImportProcessFailed)
    } else {
        Ok(())
    }
}
