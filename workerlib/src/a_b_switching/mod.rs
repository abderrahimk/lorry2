use futures::TryFutureExt;
use std::{
    fs,
    path::{Path, PathBuf},
};
use thiserror::Error;

use logging::{debug, progress};

pub(crate) mod create_workspace;

pub(crate) use create_workspace::prepare_repos;

use crate::{Arguments, LorrySpec, PushToMirrorError};

const COUNT_FILE_NAME: &str = "lorry-update-count";

/// Handles the internal working directory's A/B switching.
///
/// Prevents the caller from accessing the internals of the temporary and active (i.e stable) repo.
/// To operate on the repositories, the caller must pass in the operation to call on the internal repos.
/// `Workspace` will then also handle the accompanying management operations like updaing count files and keeping track of which is temporary or active.
/// Ensures that, should there be some failure during the mirroring process, the active(i.e garuanteed up-to-date) repo will be untouched.
/// Once the mirroring operation is done, the temporary directory becomes the active as it is more up to date, and should be pushed to the downstream if applicable.
pub(crate) struct Workspace<'a> {
    name: &'a str,
    path: PathBuf,
    temp_repo: InternalGitDirectory,
    active_repo: InternalGitDirectory,
    next_update_count: u32,
    backup_repo: PathBuf,
}

impl Workspace<'_> {
    /// Takes some operation that operates on a single internal repo.
    /// Executes this operation on the repository currently marked as temporary.
    /// Then, updates the count files and swaps which is counted as tempoary and which as active.
    /// Should a failure occur in the passed-in operation, will take a backup.
    ///
    ///  Should updating the count, or taking a backup, fail, it will return the appropriate error.
    pub async fn run_and_swap(
        &mut self,
        lorry_spec: &LorrySpec,
        arguments: &Arguments,
    ) -> Result<(), crate::MirroringError> {
        crate::try_mirror(lorry_spec, self.name, &self.temp_repo, arguments, &self.path)
        .and_then(|_| async{
            //update update count file with new count
            fs::write(
                self.temp_repo.count_file(),
                format!("{}\n", self.next_update_count),
            )
            .map_err(|e| crate::MirroringError::CouldNotUpdateCountFile {
                source: e,
                file_path: self.temp_repo.count_file(),
            })
        }).or_else(|mirror_e|async {
            //report if an error occured and copy the active to a backup, if possible     
            let error_msg = if self.active_repo.exists() {
                if let Err(e) = fs::rename(&self.temp_repo, &self.backup_repo){
                    return Err(crate::MirroringError::CouldNotMoveFailedRepoToBackup{source:e,from:self.temp_repo.clone(),to: self.backup_repo.to_path_buf(),mirror_error:Box::new(mirror_e)});
                }else{
                format!("Mirror of {} failed. The state before the mirror is at {}, and the state after the mirror is at {}.",
                    &self.name, &self.active_repo,& self.backup_repo.display())
                }
            }else{
                 format!("Mirror of {} failed. There is no backup of the state before or after the mirror.",
                &self.name, )
            };

            debug(&error_msg,&arguments.transmitter).await;
            Err(mirror_e)
        }).await?
        ;

        //switch repos
        std::mem::swap(&mut self.temp_repo, &mut self.active_repo);
        self.next_update_count += 1;

        Ok(())
    }

    /// Runs the given operation on the active repo.
    ///
    /// Given that the active repo is supposed to be ***stable***, the operation shouldn't modify the passed repo.
    ///
    /// TODO given that this is really just for the pushing function, wouldn't it make sense to just move the pushing function here instead and avoid the promise the caller doesn't modify the active?
    /// No, becuase pushing is not part of the workspace's logical responsibilities
    pub(crate) async fn with_active(
        &self,
        lorry_spec: &LorrySpec,
        arguments: &Arguments,
    ) -> Result<(), PushToMirrorError> {
        crate::push_to_mirror_server(lorry_spec, self.name, &self.active_repo, arguments).await
    }
}

#[derive(Error, Debug)]
pub enum EnsureGitRepoError {
    /// tried to initilaise a `git` repository inside a created folder, but an error occurred. Contains the output of the command that failed.
    #[error("Could not create the repository")]
    CouldNotInitializeRepo {
        source: utils::command_wrapper::CommandExecutionError,
        location: PathBuf,
    },

    /// tried to create a folder inside the working directory to hold a git repo but was unable to.
    #[error("Could not create the folder for the repo to stay in")]
    CouldNotCreateRepoFolder { source: std::io::Error },
}

//TODO would prefer the path wrappers to be private! But they are included in some errors...
/// A path that is supposed to point to one of the internal repos of a workspace.
#[derive(Debug, Clone)]
pub struct InternalGitDirectory(PathBuf);

impl InternalGitDirectory {
    fn objects_folder(&self) -> GitObjectsFolder {
        GitObjectsFolder(self.0.join("objects"))
    }

    fn lfs_objects_folder(&self) -> GitObjectsFolder {
        GitObjectsFolder(self.0.join("lfs").join("objects"))
    }
    pub(crate) fn exists(&self) -> bool {
        self.0.exists()
    }

    pub(crate) fn get_lfs_object_dir(&self, shasum: &str) -> PathBuf {
        self.lfs_objects_folder()
            .0
            .join(&shasum[0..2])
            .join(&shasum[2..4])
    }

    pub(crate) fn parent(&self) -> &Path {
        self.0
            .parent()
            .expect("Git Repository is outside of working area!")
    }

    fn get_file_inside(&self, relative_path: &str) -> PathBuf {
        self.0.join(relative_path)
    }

    pub(crate) fn count_file(&self) -> CountFile {
        CountFile(self.0.join(COUNT_FILE_NAME))
    }

    /// Checks if the internal git repository passed in actually exists, and if not, create it.
    /// Return true if we did create the repo, or false if it already existed.
    pub async fn ensure_exists(
        &self,
        verbose_logging: bool,
        transmitter: &logging::Sender,
    ) -> Result<bool, EnsureGitRepoError> {
        //TODO given the only use of transmitter is for this fn copied all over the place, what if we move it up so that it is held by Arguments?
        let debug = |s: String| async move { logging::debug(&s, transmitter).await };

        if !self.exists() {
            progress(
                format!("creating git repo {}", self),
                verbose_logging,
                transmitter,
            )
            .await;
            fs::create_dir_all(self)
                .map_err(|e| EnsureGitRepoError::CouldNotCreateRepoFolder { source: e })?;

            utils::command_wrapper::CommandBuilder::new("git")
                .args(&["init", "--bare", "-b", crate::DEFAULT_BRANCH_NAME])
                .execute(self, debug)
                .await
                .map_err(|e| EnsureGitRepoError::CouldNotInitializeRepo {
                    source: e,
                    location: std::env::current_dir().unwrap(),
                })?;
            Ok(true)
        } else {
            progress("updating existing repo", verbose_logging, transmitter).await;
            Ok(false)
        }
    }
}

impl AsRef<Path> for InternalGitDirectory {
    fn as_ref(&self) -> &Path {
        &self.0
    }
}

impl std::fmt::Display for InternalGitDirectory {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.display().fmt(f)
    }
}

#[derive(Debug, Clone)]
/// A path that is supposed to point to one of the workspace's internal repo's `git` `objects` folder.
pub struct GitObjectsFolder(PathBuf);
impl GitObjectsFolder {
    fn exists(&self) -> bool {
        self.0.exists()
    }

    fn get_file_inside(&self, relative_path: &str) -> PathBuf {
        self.0.join(relative_path)
    }
}

impl AsRef<Path> for GitObjectsFolder {
    fn as_ref(&self) -> &Path {
        &self.0
    }
}

/// Path to what is supposed to be the file that tracks the revision number of one of the workspace's internal repos.
#[derive(Debug, Clone)]
pub struct CountFile(PathBuf);

impl AsRef<Path> for CountFile {
    fn as_ref(&self) -> &Path {
        &self.0
    }
}
